
# build stage
FROM node:lts-alpine as build-stage
# Install python/pip
ENV PYTHONUNBUFFERED=1
RUN apk add --update --no-cache pkgconfig python3 && ln -sf python3 /usr/bin/python
RUN apk add --no-cache \
    python \
    g++ \
    build-base \
    cairo-dev \
    jpeg-dev \
    pango-dev \
    musl-dev \
    giflib-dev \
    pixman-dev \
    pangomm-dev \
    libjpeg-turbo-dev \
    freetype-dev \
    git
RUN git clone https://github.com/jhipster/jdl-studio.git && cd jdl-studio
WORKDIR /jdl-studio
RUN npm install
EXPOSE 3000
CMD ["npm", "start"]  
